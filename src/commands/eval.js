/**
 * 
 * @param {Object} client Pass your bot's client to this parameter to remove sensitive information.
 * @param {any} args To be evaled
 * @async
 * @returns {any} Result of the eval
 * @example commands.eval(client, '1 + 1');
 */

async function e(client, message, args) {
  let evaled;
  const message = message;
  try {
    evaled = await eval(args);
  } catch (err) {
    return err;
  }

  if (typeof evaled === 'string') {
    evaled = evaled.replace(client.token, '[TOKEN]');
  }


  if (typeof evaled === 'object') {
    evaled = require('util').inspect(evaled, {depth: 0});
  }
  if (evaled == undefined) {
    evaled = 'undefined';
  }
  
  return evaled;
}

module.exports = e;