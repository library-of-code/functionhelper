/**
 * @param {string} method The method to be used, can be: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
 * @param {string} url The URL for the request to be sent to
 * @param {string} authorization Authorization needed to access the endpoint, if needed
 * @param {Object} body - The body of the request, if any
 * @returns {Object} The result of the request
 * @example http.request('get', 'https://sas.libraryofcode.ml', 'mytoken', {type: text/css});
 */

async function request(method, url, authorization, body) {
  const axios = require('axios');
  const m = await axios({
    method: method,
    url: url,
    headers: {
      Authorization: authorization,
      authorization: authorization
    },
    body: body
  });
  return m;
} 

module.exports = request;